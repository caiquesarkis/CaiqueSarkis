const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );
scene.background = new THREE.Color(0x000);



camera.position.z = 5;
// Variables
//var ctx = document.getElementById('canvas').getContext('2d');
let colors = [0x03071e]
let running = false;
let stepsPerFrame = 25;
let dt = 0.001;
let radius = 0.2;
let oldPosition = [new THREE.Vector3(3,1.5,0),new THREE.Vector3(0,0,0),new THREE.Vector3(0,0,0)];
let position = [new THREE.Vector3(2,1.5,0),new THREE.Vector3(-3,0,2.1),new THREE.Vector3(0,-1.5,-3)];
let velocity = [new THREE.Vector3(),new THREE.Vector3()];
let aceleration = [new THREE.Vector3(),new THREE.Vector3()];



// Objects

// Grid
const size = 50;
const divisions = 50;
const gridHelper = new THREE.GridHelper( size, divisions ,0xd00000,0x370617);
gridHelper.rotation.x -= Math.PI/2 
//scene.add( gridHelper );


// LJ Spheres
const geometry = new THREE.BoxGeometry(0.3,1,0.1);
const material = new THREE.MeshStandardMaterial({color:0xfffff,wireframe:false});
material.roughness = 0.5;
material.metalness = 0.75;
//const material = new THREE.MeshBasicMaterial({color:0x515052,wireframe:false});


const sphere1 = new THREE.Mesh(geometry,material);
const sphere2 = new THREE.Mesh(geometry,material);
const sphere3 = new THREE.Mesh(geometry,material);

scene.add(sphere1);
scene.add(sphere2);
scene.add(sphere3);


sphere1.position.copy(position[0]);
sphere2.position.copy(position[1]);
sphere3.position.copy(position[2]);


const light = new THREE.PointLight( 0xffffff, 3, 50 );
light.position.set( 10, 5, 5 );
scene.add( light );


// Functions





function reset(){
    
    oldPosition = [new THREE.Vector3(0,0,0),new THREE.Vector3(0,0,0)];
    position = [new THREE.Vector3(0,0,0),new THREE.Vector3(0,0,0)];
    velocity = [new THREE.Vector3(),new THREE.Vector3()];
    aceleration = [new THREE.Vector3(),new THREE.Vector3()];
    
}

function rotateObject(speed,object1,object2,object3){
    const rotationSpeed = speed;
    object1.rotation.y += rotationSpeed;
    object2.rotation.y += rotationSpeed;
    object3.rotation.y -= rotationSpeed;
    object1.rotation.x += rotationSpeed;
    object2.rotation.x -= rotationSpeed;
    object3.rotation.x += rotationSpeed;
}

function distance(position){
    let dy = position[0].y - position[1].y 
    let dx = position[0].x - position[1].x 
    return Math.sqrt(dx*dx + dy*dy)
}


animate();

function animate() {
        rotateObject(0.01,sphere1,sphere2,sphere3)
        requestAnimationFrame(animate)  
    
	renderer.render( scene, camera );
}



