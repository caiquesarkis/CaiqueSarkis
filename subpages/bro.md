---

Autor : Caique Sarkis
title: "Ondas_de_Matéria"
output: html

---



## Hipótese de Broglie:

Broglie propôs que o comportamento dual, isto é, onda-partícula, da radiação também se aplicava à matéria. Assim como o fóton tem associada a ele uma onda luminosa que governa seu movimento, também uma partícula material ( por exemplo , um elétron) tem associada a ela uma onda de matéria que governa seu movimento

## Relação de Broglie

De acordo com de Broglie, tanto para a matéria quanto para a radiação a energia total E está relacionada à frequência v da onda associada ao seu movimento pela equação

$$
E = h\cdot\nu
$$

e o momento p é ralcionado com o comprimento de onda $\lambda$ da onda associada pela equação

$$
p = \frac{h}{\lambda}
$$

Aqui conceitos relativos a partículas, energia E em momento **p**, estão ligados através da constante de Planck  **h** aos conceitos relativos a ondas, frequência **v** e comprimento de onda **lambda**. Podemos então reescrever a equação  na forma

$$
                                                              \lambda = \frac{h}{p}
$$

e é chamda de relação de Broglie.

Exemplo

Qual o comprimento de onda de de Broglie de uma bola de beisebal se movendo com uma velocidade v = 10 m/s ?

$$
m = 1,0_{kg}
$$

$$
\lambda = \frac{h}{p} = \frac{h}{mv} = \frac{6,6\cdot10^{-34}}{1\cdot10} = 6,6 \cdot 10^{-25}A^{°}$
$$

## Reflexão de Bragg

Foi Elsasser quem mostrou, em 1926, que a natureza ondulatória da matéria poderia ser testada  da mesma forma que a natureza ondulatória dos raios X havia sido, ou seja, fazendo-se com que um feixe de elétrons de energia apropriada incida sobre um sólido cristalino. Os átomos de cristal agem como um arranjo tridimensional de centros de difração para a onda eletrônica, espalhando fortemente os elétrons em certas direções características, exatamente como na difração de raios X. Esta idéia foi confirmada por experiências feitas por Davisson e Germer. Elétrons emitidos por um filamento aquecido são acelerados através de uma diferença de potencial V e emergem do "canhão de elétrons" com energia cinética $$e\cdot V$$.

Esse elétron penetra no arranjo cristalino e como o momento da partícula é baixo acaba por refletir-se em maior parte na primeira camada

Utilizando geometria podemos perceber que

$$
                                 cos(90° - \phi) - \frac{l}{d} = sin(\phi)
$$

onde l é a diferença dos caminhos óticos e $\phi$  é o angulo de incidência do elétron. Como a diferença dos caminhos é dobrada pela reflexão, temos

$$
\lambda = 2dsin(\phi)
$$

![sdifsa](https://s3.us-west-2.amazonaws.com/secure.notion-static.com/f66ac82a-5868-4baa-9a0b-2670688e0a1c/bragg.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20210325%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20210325T070721Z&X-Amz-Expires=86400&X-Amz-Signature=d9551cef6dfe5180cca905f7ffe3ba060afb9ff35b8c7bae9dd92aa9f882dce5&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22bragg.png%22)

## Conclusão

Não apenas elétrons, mas todos  os objetos materiais, carregados ou não, apresentam características ondulatórias em seu movimento, quando estão sob as condiões da ótica física. Os aspectos ondulatórios se tornam dificilmente observaveis para comprimentos de onda muito pequenos como no caso de matéria macroscópica e por isso passam despercebidamente.

## Referências

Física Quântica - Eisberg e Resnick

Vídeo sobre ondas de matéria: https://www.youtube.com/watch?v=M1BbGGNzBz8